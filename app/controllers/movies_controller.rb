
class MoviesController < ApplicationController
  def index
    @movies = Movie.all
  end

  def show
    id = params[:id] # 1 or whaterver movie id is passed
    @movie = Movie.find(id)
  end

  def new
    # Default: render «new» template
  end

  def create
    @movie = Movie.create! params[:movie]
    flash[:notice] = "#{@movie.title} was succesfully created!"
    redirect_to movies_path
  end

  def edit
    @movie = Movie.find params[:id]
  end

  def update
    @movie = Movie.find params[:id]
    @movie.update_attributes! params[:movie]
    flash[:notice] = "#{@movie.title} was updated!"
    redirect_to movies_path
  end

  def destroy
    @movie = Movie.find params[:id]
    title  = @movie.title
    @movie.destroy
    flash[:notice] = "'#{title}' was deleted!"
    redirect_to movies_path
  end
end
