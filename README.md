# SaaSTV: My Rotten Potatoes #
Basic Rails app result from the guided introduction practice to CRUD concepts
in Rails.

## Reviewed material ##
* Setting up a new project with `rails new`, editing `GemFile` and using `bundle
  install`.
* Setting a new root route and add resourses in `routes.rb`.
* Initialize the `rails server` to start live development and update the routes
  with `rake routes`.
* Perform a migration with `rails generate migration`, editing the source and
  using `rake db:migrate`.
* Create the model inheriting from `ActiveRecord::Base` and using
  `attr_accessible` attributes.
* Create the controller and its actions to interact with the model.
* Create the layouts using HAML templates and calling route's methods to
  pass and retrieve information using the controller.
* Use the assets to apply styles to views.
* Display notices and warnings with `flash`.
* Use `button_to` to call route methods.
* Set breakpoints and use `rails server --debugger` to check the values of the
  objects at a given place.

---
The code is based on the original work of Armando Fox and David Patterson that
can be found in [SaaSTV screencasts][TV] and is used and distributed under the
[Attribution Non-Commercial 3.0 CC License][CC].

[TV]: https://www.youtube.com/playlist?list=PLjbL0BCR04Q13ph3TLp3dPsYQi7ujH6R4
[CC]: https://creativecommons.org/licenses/by-nc/3.0/